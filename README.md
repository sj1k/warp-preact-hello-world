Basic hello world example for `warp` as a backend and `preact / htm` as the frontend.

No npm nonsense required. The [preact](https://github.com/preactjs/preact) and [htm](https://github.com/developit/htm) files are included.
