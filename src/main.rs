use warp::Filter;

#[macro_use]
use tokio;

#[tokio::main]
async fn main() {
    let homepage = warp::get()
        .and(warp::path::end())
        .and(warp::fs::file("./web/static/index.html"));

    let scripts = warp::path("src").and(warp::fs::dir("./web/static/"));
    let routes = homepage.or(scripts);

    println!("serving...");
    warp::serve(routes).run(([127, 0, 0, 1], 3030)).await;
}
